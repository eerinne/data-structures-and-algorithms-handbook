# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Front Desk Technical Support

## Lenie Jane Tinagsa

👋 Aspiring Front Desk Technical Support! 🚀👨‍💻 — 💌 leniejanetinagsa@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_tinagsa_leniejane.jpg](<images/act_2_tinagsa_leniejane.jpg>)

### Bio

**Good to know:** I love art things and I love the country of Japan specially their animations.  

**Motto:** Train your mind to see the good in every situation.

**Languages:** Python, Java; Japanese, Portuguese, English (just the basics).

**Other Technologies:** Microsoft Word and Eclipse.

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/b8d627c287fdc)

<!-- END -->
